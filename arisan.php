<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body>
    <!-- <content> -->
    <div class="navbar-fixed">
    <nav class="nav-extended zn-bg-color-dark">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Arisan</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
        <div class="nav-content zn-container">
            <ul class="tabs tabs-transparent tabs-fixed-width">
                <li class="tab"><a href="#arisan-info">INFO</a></li>
                <li class="tab"><a href="#arisan-event">EVENT</a></li>
                <li class="tab"><a class="active" href="#arisan-status">STATUS</a></li>
            </ul>
        </div>
    </nav>
    </div>

    <ul id="slide-out" class="sidenav">
        <li>
            <div class="user-view zn-bg-color-dark">
                <div class="zn-label-sidenav">
                    GMIClub V1.8.0
                </div>
                <a href="#user"><img class="circle" src="img/img1.jpg"></a>
                <a href="#name"><h4 class="white-text zn-text-medium zn-mb-0">Hamdan Muttaqin</h4></a>
                <a href="#email"><h6 class="white-text zn-text-medium zn-m-0 zn-pb-20" style="opacity: 0.5;">XXXXX</h6></a>
                <a class="zn-icon-set">
                    <img src="img/ic_side.png" alt="" srcset="">
                </a>
            </div>
        </li>
        <li><a href="#!">
            <!-- <i class="material-icons">free_breakfast</i> -->
            <img class="zn-icon-side-kopi" src="img/arisan.png" alt="" srcset="">
        Arisan</a></li>
        <li><a href="#!"><i class="material-icons">credit_card</i>Transaksi</a></li>
        <li>
            <div class="divider"></div>
        </li>
      
        <li><a href="#!"><i class="material-icons-outlined">lock</i>Logout</a></li>
    </ul>


    <div id="arisan-info" class="col s12 zn-container zn-pt-60">
        <div class="zn-border-bottom zn-p-content">
            <h5 class="zn-text-regular zn-text-black zn-m-0 zn-pb-5">Info Cicilan Arisan</h5>
            <h6 class="zn-text-regular zn-text-light zn-m-0 small">Periode September, 2019</h6>
        </div>
        <!-- LIST GRAM -->
        <div class="row zn-p-content zn-m-0 zn-m-0 zn-pt-0">
            <div class="row zn-list-gram zn-border-bottom-1">
                <div class="col s7">
                    <div class="zn-label-white">5 Gram</div>
                </div>
                <div class="col s1 zn-text-right">
                    <h6> Rp.</h6>
                </div>
                <div class="col s4 zn-text-right">
                    <h6>300.000</h6>
                </div>
            </div>
            <div class="row zn-list-gram zn-border-bottom-1">
                <div class="col s7">
                    <div class="zn-label-white">10 Gram</div>
                </div>
                <div class="col s1 zn-text-right">
                    <h6> Rp.</h6>
                </div>
                <div class="col s4 zn-text-right">
                    <h6>734.000</h6>
                </div>
            </div>

            <div class="row zn-list-gram zn-border-bottom-1">
                <div class="col s7">
                    <div class="zn-label-white">25 Gram</div>
                </div>
                <div class="col s1 zn-text-right">
                    <h6> Rp.</h6>
                </div>
                <div class="col s4 zn-text-right">
                    <h6>1.700.000</h6>
                </div>
            </div>

            <div class="row zn-list-gram zn-border-bottom-1">
                <div class="col s7">
                    <div class="zn-label-white">50 Gram</div>
                </div>
                <div class="col s1 zn-text-right">
                    <h6> Rp.</h6>
                </div>
                <div class="col s4 zn-text-right">
                    <h6>3.600.000</h6>
                </div>
            </div>

            <div class="row zn-list-gram zn-border-bottom-1">
                <div class="col s7">
                    <div class="zn-label-white">100 Gram</div>
                </div>
                <div class="col s1 zn-text-right">
                    <h6> Rp.</h6>
                </div>
                <div class="col s4 zn-text-right">
                    <h6>7.300.000</h6>
                </div>
            </div>

            <div class="col s12 zn-mt-button">
                <span class="zn-text-color zn-notif">Silahkan melakukan pendaftaran registrasi untuk dapat menikmati layanan Arisan LM GMI Club</span>
                <!-- <a class="waves-effect waves-light btn zn-button zn-text-medium">DAFTAR ARISAN LM</a> -->
            </div>
        </div>

    </div>
    <div id="arisan-event" class="zn-container zn-pt-60">

        <div class="card z-depth-0">
            <div class="card-image">
                <img src="img/img1.jpg">
                <a class="btn-floating btn-large halfway-fab waves-effect waves-light">
                    <i class="material-icons-outlined">near_me</i></a>
            </div>
            <div class="card-content zn-p-0 zn-pb-40">
                <div class="zn-border-bottom zn-p-content">
                    <h5 class="zn-text-medium zn-m-0 zn-pb-5">Arisan LM</h5>
                    <h6 class="small zn-text-regular zn-m-0 zn-text-light">Periode September, 2019</h6>
                </div>
                <div class="zn-p-content zn-mb-0 zn-pb-10 zn-pt-10 row zn-border-bottom-1">
                    <div class="col s1">
                        <i class="material-icons zn-text-color-favorite">favorite</i>
                    </div>
                    <div class="col s10 zn-pl-25">
                        <h6 class="zn-text-regular zn-m-0 zn-pb-5 zn-text-color-favorite">Free Tickets
                            Available</h6>
                        <h6 class="small zn-text-regular zn-m-0 zn-text-light">only for 100 members</h6>
                    </div>

                </div>
                <div class="zn-p-content zn-mb-0 zn-pb-10  zn-pt-10 row zn-border-bottom-1">
                    <div class="col s1">
                        <i class="material-icons zn-text-light">watch_later</i>
                    </div>
                    <div class="col s10 zn-pl-25">
                        <h6 class="zn-text-regular zn-m-0 zn-pb-5">Friday, 20 September 2019</h6>
                        <h6 class="small zn-text-regular zn-m-0 zn-text-light">16:00</h6>
                    </div>

                </div>
                <div class="zn-p-content zn-mb-0 zn-pb-10 zn-pt-10 row zn-border-bottom-1">
                    <div class="col s1">
                        <i class="material-icons zn-text-light">room</i>
                    </div>
                    <div class="col s10 zn-pl-25">
                        <h6 class="zn-text-regular zn-m-0 zn-pb-5">Coffee Toffee Gasibu</h6>
                        <h6 class="small zn-text-regular zn-m-0 zn-text-light">Jl.Surapati No.37 Bandung
                        </h6>

                    </div>

                </div>
                <div class="zn-p-content zn-mb-0 zn-pb-10 zn-pt-10 row zn-border-bottom-1">
                    <div class="col s1">
                        <i class="material-icons zn-text-light">star</i>
                    </div>
                    <div class="col s10 zn-pl-25">
                        <h6 class="zn-text-regular zn-m-0 zn-pb-5">Tema</h6>
                        <h6 class="small zn-text-regular zn-m-0 zn-text-light">Healthy Food & happy
                            Cooking</h6>
                    </div>

                </div>

                <div class="zn-p-content zn-mb-0 zn-pb-10 zn-pt-10 row zn-border-bottom-1">
                    <div class="col s1">
                        <!-- <i class="material-icons zn-text-light">star</i> -->
                        <img src="img/baju.png" alt="" srcset="">
                    </div>
                    <div class="col s10 zn-pl-25">
                        <h6 class="zn-text-regular zn-m-0 zn-pb-5">Dresscode</h6>
                        <h6 class="small zn-text-regular zn-m-0 zn-text-light">Orange</h6>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div id="arisan-status" class="col s12 zn-bg-light zn-pt-60">
        <!-- LIST STATUS -->

        <div class="con-statut-count zn-bg-white z-depth-1">
            <div class="zn-container">
                <a href="#" class="zn-foot-login">
                    <h4 class="zn-text-color zn-text-medium zn-m-0">0</h4>
                    <h6 class="zn-text-11 zn-text-light zn-text-regular">ACTIVE</h6>
                </a>
                <div class="zn-separat-status"></div>
                <a href="#" class="zn-foot-login">
                    <h4 class="zn-text-color zn-text-medium zn-m-0">0</h4>
                    <h6 class=" zn-text-11 zn-text-light zn-text-regular">CLOSED</h6>
                </a>
            </div>
        </div>
        <div class="zn-con-sort ">
            <div class="zn-container">
                <div class="zn-sort-label">
                    SORT BY
                </div>
                <a class="zn-sort zn-sort-active">
                    KELOMPOK
                </a>
                <a class="zn-sort">
                    GRAM
                </a>
            </div>

        </div>
        <div class="zn-container zn-bg-white">



            <div class="row zn-p-content zn-m-0 zn-pt-5">
                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">5 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">On Booking
                                <span class="zn-label-booked zn-status-btn">booked</span>
                            </h6>
                        </div>
                        <h6 class="zn-m-0 zn-text-light zn-text-11">Booking Code #2452</h6>
                    </div>
                </div>

                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">10 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">Kelompok 1355
                                <span class="zn-label-closed zn-status-btn">Closed</span>
                            </h6>
                        </div>

                        <h6 class="zn-m-0 zn-text-light zn-text-11">start Jun 2019 end Mar 2020</h6>

                        <div class="zn-con-point zn-pb-5">
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-yellow"></div>
                            <div class="zn-point-yellow"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                        </div>
                        <h6 class="zn-text-color zn-m-0 zn-text-11">Anda Sudah Menang di bulan Juli 2019</h6>
                    </div>
                </div>

                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">100 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">Kelompok 1355
                                <span class="zn-label-active zn-status-btn">Active</span>
                            </h6>
                        </div>
                        <h6 class="zn-m-0 zn-text-light zn-text-11">start Jun 2019 end Mar 2020</h6>

                        <div class="zn-con-point zn-pb-5">
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                            <div class="zn-point"></div>
                        </div>
                        <h6 class="zn-text-color zn-m-0 zn-text-11">Anda belum menang di bulan ini</h6>
                    </div>
                </div>

                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">5 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">Kelompok 967
                                <span class="zn-label-active zn-status-btn">Active</span>
                            </h6>
                        </div>
                        <h6 class="zn-m-0 zn-text-light zn-text-11">start Jun 2019 end Mar 2020</h6>

                        <div class="zn-con-point zn-pb-5">
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                        </div>
                        <h6 class="zn-text-color zn-m-0 zn-text-11">Anda sudah menang di bulan agustus 2018</h6>
                    </div>
                </div>

                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">5 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">Kelompok 967
                                <span class="zn-label-active zn-status-btn">Active</span>
                            </h6>
                        </div>
                        <h6 class="zn-m-0 zn-text-light zn-text-11">start Jun 2019 end Mar 2020</h6>

                        <div class="zn-con-point zn-pb-5">
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                        </div>
                        <h6 class="zn-text-color zn-m-0 zn-text-11">Anda sudah menang di bulan agustus 2018</h6>
                    </div>
                </div>

                <div class="row zn-list-gram zn-border-bottom-1">
                    <div class="col s3">
                        <div class="zn-label-white">5 Gram</div>
                    </div>
                    <div class="col s9 zn-pl-25">
                        <div>
                            <h6 class="zn-m-0 zn-pb-5 zn-text-regular">Kelompok 967
                                <span class="zn-label-active zn-status-btn">Active</span>
                            </h6>
                        </div>
                        <h6 class="zn-m-0 zn-text-light zn-text-11">start Jun 2019 end Mar 2020</h6>

                        <div class="zn-con-point zn-pb-5">
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                            <div class="zn-point-active"></div>
                        </div>
                        <h6 class="zn-text-color zn-m-0 zn-text-11">Anda sudah menang di bulan agustus 2018</h6>
                    </div>
                </div>


            </div>


        </div>
    </div>

    <!-- </content> -->

    <!-- <div class="zn-fit-bottom">
        <ul class="tabs tabs-fixed-width">
            <li class="tab"><a href="#test1">Test 1</a></li>
            <li class="tab"><a class="active" href="#test2">Test 2</a></li>
        </ul>
    </div> -->

    <div class="zn-fit-bottom zn-navbot">
        <ul class="tabs tabs-fixed-width">
            <li class="tab">
                <a href="#test1">
                    <div class="zn-navbot-icon">
                        <img class="zn-custom-img" src="img/arisan.png" alt="" srcset="">
                    </div>
                    <!-- <i class="material-icons zn-navbot-icon">credit_card</i> -->
                    <span class="zn-navbot-text" style="margin-top: -35px !important;">Arisan</span>

                </a>
            </li>
            <li class="tab">
                <a class="active" href="#test2">
                    <i class="material-icons zn-navbot-icon">credit_card</i>
                    <span class="zn-navbot-text">Transaksi</span>
                </a>
            </li>
        </ul>
    </div>




    <?php include 'footer.php'; ?>
</body>

</html>