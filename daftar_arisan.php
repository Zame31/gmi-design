<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body>
    <!-- <content> -->
    <div class="navbar-fixed">
        <nav class="nav-extended zn-bg-color-dark z-depth-0">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo zn-text-white" style="left: 50%;">Darfar Arisan LM</a>
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i
                        class="material-icons zn-text-white">arrow_back</i></a>
            </div>
        </nav>
    </div>


    <div class="zn-con-form zn-pb-15">
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Pilih Gramasi</h6>
    </div>

    <div class="row">
        <div class="col s12">
            <div class="zn-pilih-gram">
                <i class="material-icons zn-text-color zn-icon-gram">check_circle</i>
                <h3 class="zn-text-color zn-bold zn-mtb-10">5</h3>
                <div class="zn-text-color zn-text-11">gram</div>
            </div>

            <div class="zn-pilih-gram zn-normal">
                <img src="img/icon_gram.png" alt="" srcset="">
                <h3 class="zn-text-light zn-bold zn-mtb-10">10</h3>
                <div class="zn-text-light zn-text-11">gram</div>
            </div>

            <div class="zn-pilih-gram zn-normal">
                <img src="img/icon_gram.png" alt="" srcset="">
                <h3 class="zn-text-light zn-bold zn-mtb-10">25</h3>
                <div class="zn-text-light zn-text-11">gram</div>
            </div>

            <div class="zn-pilih-gram zn-normal">
                <img src="img/icon_gram.png" alt="" srcset="">
                <h3 class="zn-text-light zn-bold zn-mtb-10">50</h3>
                <div class="zn-text-light zn-text-11">gram</div>
            </div>

            <div class="zn-pilih-gram zn-normal">
                <img src="img/icon_gram.png" alt="" srcset="">
                <h3 class="zn-text-light zn-bold zn-mtb-10">100</h3>
                <div class="zn-text-light zn-text-11">gram</div>
            </div>
        </div>

    </div>

    <div class="zn-con-form zn-pb-15">
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Pilih Periode</h6>
    </div>

    <div class="row">
        <div class="col s12">
            <div class="zn-pilih-gram">
                <i class="material-icons zn-text-color zn-icon-gram">check_circle</i>
                <h3 class="zn-text-color zn-bold zn-mtb-10">Sep</h3>
                <div class="zn-text-color zn-text-11">2019</div>
            </div>
            <div class="zn-pilih-gram zn-normal">
                <i class="material-icons zn-text-light zn-icon-gram">date_range</i>
                <h3 class="zn-text-light zn-bold zn-mtb-10">Oct</h3>
                <div class="zn-text-light zn-text-11">2019</div>
            </div>

            <div class="zn-pilih-gram zn-normal">
                <i class="material-icons zn-text-light zn-icon-gram">date_range</i>
                <h3 class="zn-text-light zn-bold zn-mtb-10">Nov</h3>
                <div class="zn-text-light zn-text-11">2019</div>
            </div>

        </div>

    </div>

    <div class="row zn-mlr-10">
        <div class="col s12">
            <p>
                <label>
                    <input type="checkbox" class="filled-in" checked="checked" />
                    <span>
                    </span>
                </label>

                <span class="zn-text-11" style="position: absolute;padding: 3px 0px;">Setuju dengan <span class="zn-text-color zn-text-medium">Syarat &
                            Ketentuan</span> yang berlaku
                    </span>
            </p>
        </div>
    </div>


    
    <div class="zn-fit-bottom" style="background: #8648f4;height: 55px;padding: 10px;">
        <div>
            <h6 class="zn-text-medium zn-text-center ">DAFTAR</h6>
        </div>
    </div>




    <?php include 'footer.php'; ?>
</body>

</html>