<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body class="zn-bg-light">
    <!-- <content> -->
    <div class="navbar-fixed">
    <nav class="nav-extended zn-bg-color">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo zn-text-white" style="display: block;float: right;text-align: right;position: absolute;right: -220px;">
            1.600.000
        </a>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i
                    class="material-icons zn-text-white">arrow_back</i></a>
        </div>
        <div class="nav-content zn-container">
            <ul class="tabs tabs-transparent tabs-fixed-width">
                <li class="tab"><a href="#arisan-info">TAGIHAN</a></li>
                <li class="tab"><a href="#arisan-event">PEMBAYARAN</a></li>
            </ul>
        </div>
    </nav>
</div>

    <ul id="slide-out" class="sidenav">
        <li>
            <div class="user-view zn-bg-color-dark">
                <div class="zn-label-sidenav">
                    GMIClub V1.8.0
                </div>
                <a href="#user"><img class="circle" src="img/img1.jpg"></a>
                <a href="#name"><h4 class="white-text zn-text-medium zn-mb-0">Hamdan Muttaqin</h4></a>
                <a href="#email"><h6 class="white-text zn-text-medium zn-m-0 zn-pb-20">XXXXX</h6></a>
                <a class="zn-icon-set"><i class="material-icons">filter_none</i></a>
            </div>
        </li>
        <li><a href="#!">
            <!-- <i class="material-icons">free_breakfast</i> -->
            <img class="zn-icon-side-kopi" src="img/arisan.png" alt="" srcset="">
        Arisan</a></li>
        <li><a href="#!"><i class="material-icons">credit_card</i>Transaksi</a></li>
        <li>
            <div class="divider"></div>
        </li>
      
        <li><a href="#!"><i class="material-icons-outlined">lock</i>Logout</a></li>
    </ul>


    <div id="arisan-info" class="col s12 zn-container" style="padding-top: 125px;">
    
        
    <div class="zn-fit-bayar zn-bayar">
       <div class="zn-p-content" style="padding:10px 20px !important;">
            <span class="zn-text-11 zn-text-regular zn-m-0" style="color: #ffffff7a;">TOTAL TAGIHAN</span>
            <h5 class="zn-text-medium zn-text-white zn-m-0 zn-mt-5">1.600.000</h5>
            <button class="zn-btn-bayar">DESELECTED ALL</button>
       </div>
    </div>


        <div class="zn-con-form">
            <h6 class="zn-form-label zn-color-black zn-text-medium zn-border-bottom-4 zn-mlr-20" style="padding: 13px 0;">Register Member
                <p class="zn-checkbox-tx">
                    <label>
                        <input type="checkbox" class="filled-in" checked="checked" />
                        <span style="padding: 0;"></span>
                    </label>
                </p>
            </h6>
         
            <div class=""></div>
            <div class="zn-plr-10 zn-mt-10">
                <div class="row zn-m-0 zn-pb-5">
                    <div class="col s2">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">Date</h6> 
                    </div>
                    <div class="col s4">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">02 Sep 2019</h6> 
                    </div>
                </div>
                <div class="row zn-m-0 zn-pb-5">
                    <div class="col s2">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">Date</h6> 
                    </div>
                    <div class="col s4">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">02 Sep 2019</h6> 
                    </div>
                </div>

                <div class="zn-mt-25">
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                </div>

                <div class="zn-border-top-1 zn-plr-10 zn-mt-10">
                    <div class="row zn-pb-20">
                        <div class="col s6">
                            <h6 class="zn-text-medium zn-text-right">Total</h6>
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-medium zn-text-right">100.000</h6>
                        </div>
                    </div>
                </div>
                

            </div>
           
        </div>
     
        <div class="zn-con-form">
            <h6 class="zn-form-label zn-color-black zn-text-medium zn-border-bottom-4 zn-mlr-20" style="padding: 13px 0;">Register Member
                <p class="zn-checkbox-tx">
                    <label>
                        <input type="checkbox" class="filled-in" checked="checked" />
                        <span style="padding: 0;"></span>
                    </label>
                </p>
            </h6>
         
            <div class=""></div>
            <div class="zn-plr-10 zn-mt-10">
                <div class="row zn-m-0 zn-pb-5">
                    <div class="col s2">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">Date</h6> 
                    </div>
                    <div class="col s4">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">02 Sep 2019</h6> 
                    </div>
                </div>
                <div class="row zn-m-0 zn-pb-5">
                    <div class="col s2">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">Date</h6> 
                    </div>
                    <div class="col s4">
                        <h6 class="zn-text-11 zn-text-light zn-m-0">02 Sep 2019</h6> 
                    </div>
                </div>

                <div class="zn-mt-25">
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                    <div class="row zn-m-0 zn-pb-5">
                        <div class="col s6">
                            <h6 class="zn-m-0">Cicilan Registrasi 4/5</h6> 
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-right zn-m-0">100.000</h6> 
                        </div>
                    </div>
                </div>

                <div class="zn-border-top-1 zn-plr-10 zn-mt-10">
                    <div class="row zn-pb-20">
                        <div class="col s6">
                            <h6 class="zn-text-medium zn-text-right">Total</h6>
                        </div>
                        <div class="col s6">
                            <h6 class="zn-text-medium zn-text-right">100.000</h6>
                        </div>
                    </div>
                </div>
                

            </div>
           
        </div>
     
     
    </div>
    <div id="arisan-event" class="zn-container zn-pt-60">
        <div class="zn-con-form">
            <h6 class="zn-form-label zn-color-black zn-m-0 zn-pb-0">INVOICE 5555
                <span class="zn-text-light zn-text-11 zn-float-right">10 Sep 2019</span>
            </h6>
            <h6 class="zn-form-label zn-text-light zn-m-0 zn-pt-5">RP. 1.600.000
            <span class="zn-label-active zn-status-btn zn-text-center">validating</span>
            </h6>
        </div>

        <div class="zn-con-form">
            <h6 class="zn-form-label zn-color-black zn-m-0 zn-pb-0">INVOICE 5555
                <span class="zn-text-light zn-text-11 zn-float-right">10 Sep 2019</span>
            </h6>
            <h6 class="zn-form-label zn-text-light zn-m-0 zn-pt-5">RP. 1.600.000
            <span class="zn-label-active zn-status-btn zn-text-center">validating</span>
            </h6>
        </div>

        <div class="zn-con-form">
            <h6 class="zn-form-label zn-color-black zn-m-0 zn-pb-0">INVOICE 5555
                <span class="zn-text-light zn-text-11 zn-float-right">10 Sep 2019</span>
            </h6>
            <h6 class="zn-form-label zn-text-light zn-m-0 zn-pt-5">RP. 1.600.000
            <span class="zn-label-active zn-status-btn zn-text-center">validating</span>
            </h6>
        </div>
     

    </div>
  

    <!-- </content> -->

    <!-- <div class="zn-fit-bottom">
        <ul class="tabs tabs-fixed-width">
            <li class="tab"><a href="#test1">Test 1</a></li>
            <li class="tab"><a class="active" href="#test2">Test 2</a></li>
        </ul>
    </div> -->

    
    <div class="zn-fit-bottom" style="background: #8648f4;height: 55px;padding: 10px;">
        <div>
            <h6 class="zn-text-medium zn-text-center ">PEMBAYARAN</h6>
        </div>
    </div>

    




    <?php include 'footer.php'; ?>
</body>

</html>