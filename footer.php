<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script>
    M.AutoInit();

    function znNotifOpen() {
        $(document).ready(function(){
            $('#znNotif').modal();
            $('#znNotif').modal('open'); 
        });
    }

    function znLoading() {
        $(document).ready(function(){
            $('#znLoadpage').modal({
                dismissible	: false
            });
            $('#znLoadpage').modal('open'); 
        });
    }
</script>