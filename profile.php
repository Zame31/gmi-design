<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body class="zn-bg-light zn-pb-10">
    <!-- <content> -->
    <nav class="nav-extended zn-bg-white z-depth-0">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo zn-text-black" style="left: 50%;">My Profile</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons zn-text-black">arrow_back</i></a>
        </div>
    </nav>

    <div class="zn-text-center zn-con-prof">
        <div>
        <div ><i class="material-icons zn-text-black zn-profile-foto">add_a_photo</i></div>
        </div>
        
        <h4 class="zn-text-medium">Hamdan Muttaqin</h4>
        <div class="zn-ref-code">Ref. Code 01355</div>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">NAMA LENGKAP</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">Hamdan muttaqin</h6>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">EMAIL</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">Hamdan@gmail.com</h6>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">PASSWORD</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">*******</h6>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">NO TELPON</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">Hamdan muttaqin</h6>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">ALAMAT SESUAI KTP 
            <a class="material-icons zn-text-light zn-edit-tool">edit</a>
        </span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">-</h6>
    </div>

    
    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">ALAMAT SURAT MENYURAT 
            <a class="material-icons zn-text-light zn-edit-tool">edit</a>
        </span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">-</h6>
    </div>

    
    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">REKENING 
            <a class="material-icons zn-text-light zn-edit-tool">edit</a>
        </span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">-</h6>
    </div>

    
    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">LAMPIRAN FOTO KTP 
        </span>
        <h6 class="zn-form-label zn-text-light zn-text-center zn-text-medium zn-m-0 zn-p-content">
        <i class="material-icons">add_a_photo</i>
        <span style="margin-left:10px;margin-top: 8px;display: inline-block;"> Upload</span>
        </h6>
    </div>




    <?php include 'footer.php'; ?>
</body>

</html>