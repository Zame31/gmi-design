<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body class="zn-p-0">
    <content class="v-center">
        <div class="container zn-plr-10">
            <div class="row zn-mb-head">
                <div class="col s12">
                    <img class="zn-logo" src="img/logo.png" width="80px" alt="" srcset="">
                    <h3 class="zn-text-color zn-text-medium zn-text-left zn-m-0 zn-pb-5">Welcome Back,</h3>
                    <h6 class="zn-text-light zn-text-left zn-text-regular zn-m-0">Please login to continue</h6>
                </div>
            </div>
            <div class="row">
                <form class="col s12">
                    <div class="row zn-mb-0">
                        <div class="input-field col s12">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 zn-mt-5">
                            <input id="password" type="password" class="validate">
                            <label for="password">Password</label>
                            <i class="material-icons zn-view-pass zn-text-light">remove_red_eye</i></a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col s12">
                <a class="waves-effect waves-light btn zn-button">LOGIN</a>
            </div>

            <div class="row zn-mt-15">
                
            <div class="col s12">
                <a href="#"><h6 class="zn-foot-login zn-text-color zn-text-regular">Activate</h6></a>
                <div class="zn-separat"></div>
                <a href="#"><h6 class="zn-foot-login zn-text-color zn-text-regular">Forgot Password ?</h6></a>
            </div>
            </div>
        </div>

    </content>
    
<?php include 'footer.php'; ?>
</body>
</html>