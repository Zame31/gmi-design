<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body >

<a class="waves-effect waves-light btn" onclick="znNotifOpen()">Notif</a>
<a class="waves-effect waves-light btn" onclick="znLoading()">Loading</a>


<div id="znNotif" class="modal zn-modal">
    <div class="modal-content">
      <h4 class="zn-tittle-notif">Mohon Cek Kembali</h4>
      <p class="zn-text-notif">Anda Mendaftarkan Arisan LM <br> 5 Gram <br> periode September 2019</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat zn-text-color zn-text-medium">Batal</a>

      <a href="#!" class="waves-effect waves-green btn-flat zn-text-color zn-text-medium">Lanjutkan</a>
    </div>
  </div>

  
<div id="znLoadpage" class="modal zn-modal zn-modal-fit">
    <div class="modal-content" style="padding: 3px 20px 0px">
      <p >
      <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>  
    
    <h6 class="zn-text-notif zn-text-loading">Please Wait ...</h6> 
        </p>
    </div>
  </div>

    <?php include 'footer.php'; ?>

    <script>
       
        
    </script>
</body>

</html>