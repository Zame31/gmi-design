<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
    <content class="v-center">
        <div class="container zn-plr-10">
            <div class="row zn-mb-head">
                <div class="col s12">
                    <img class="zn-text-center zn-pb-20" src="img/no_data.png" width="80px" alt="" srcset="">
                    <h3 class="zn-text-light zn-text-medium zn-text-center zn-m-0 zn-pb-5">No Data Available</h3>
                    <h6 class="zn-text-light zn-text-center zn-text-regular zn-m-0">Saat ini anda belum memiliki tagihan</h6>
                </div>
            </div>
        </div>
    </content>
    
<?php include 'footer.php'; ?>
</body>
</html>