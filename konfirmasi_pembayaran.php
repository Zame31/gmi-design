<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body class="zn-bg-light">
    <!-- <content> -->
    <div class="navbar-fixed">
        <nav class="nav-extended zn-bg-color-dark z-depth-0">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo zn-text-white" style="left: 70%;width: 100%;">Konfirmasi Pembayaran</a>
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i
                        class="material-icons zn-text-white">arrow_back</i></a>
            </div>
        </nav>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">TOTAL PEMBAYARAN</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">RP. 1.600.000
            <span>
                <img src="img/copy.png" class="zn-icon-copy-right" alt="" srcset="">
            </span>
        </h6>
    </div>

    <div class="zn-con-form">


        <span class="zn-form-label zn-text-light  zn-pb-5 zn-mlr-15 zn-text-11 zn-border-bottom-1"
            style="display: block;padding: 20px 0px;">RINCIAN PEMBAYARAN</span>

        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">Cicilan Registrasi</h6>
        <div class="row zn-mlr-15">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-light">Cicilan Registrasi 4/5</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">100.000</h6>
            </div>
        </div>

        <div class="row zn-mlr-15 zn-border-top-1">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">100.000</h6>
            </div>
        </div>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Arisan LM</h6>
        <div class="zn-mlr-15 zn-text-11 zn-text-light zn-pb-5">Aug 2019 Kel. 1354 10 gram</div>
        <div class="row zn-mlr-15 zn-pb-5">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
            </div>
        </div>
        <div class="row zn-mlr-15 zn-pb-5">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
            </div>
        </div>

        <div class="row zn-mlr-15 zn-border-top-1">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">100.000</h6>
            </div>
        </div>

        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Arisan LM</h6>
        <div class="zn-mlr-15 zn-text-11 zn-text-light zn-pb-5">Aug 2019 Kel. 1354 10 gram</div>
        <div class="row zn-mlr-15 zn-pb-5">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
            </div>
        </div>



        <div class="row zn-mlr-15 zn-border-top-1">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-light">100.000</h6>
            </div>
        </div>

        <div class="row zn-mlr-15 zn-border-top-3">
            <div class="col s6 zn-p-0">
                <h6 class="small zn-text-right zn-text-black zn-text-medium">Total</h6>
            </div>
            <div class="col s1 zn-p-0">
                <h6 class="small zn-text-right zn-text-black zn-text-medium">Rp.</h6>
            </div>
            <div class="col s5 zn-p-0">
                <h6 class="small zn-text-right zn-text-black zn-text-medium">100.000</h6>
            </div>
        </div>
    </div>


    <!-- PILIH BANK -->
    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">PILIH BANK TUJUAN</span>

        <div class="zn-mt-15">
            <div class="row zn-border-bottom-1 zn-pb-5">
                <div class="col s3">
                    <img src="img/bni.png" style="width:100%;" alt="" srcset="">
                </div>
                <div class="col s9">
                    <h6 class="zn-text-black zn-m-0 zn-pb-5">0391028
                        <p class="zn-checkbox-tx">
                            <label>
                                <input type="checkbox" class="filled-in" checked="checked" />
                                <span style="padding: 0;"></span>
                            </label>
                        </p>
                    </h6>
                    <h6 class="small zn-text-light zn-m-0">PT. Gema Mandiri Indonesia</h6>

                </div>
            </div>
            <div class="row zn-border-bottom-1 zn-pb-5">
                <div class="col s3">
                    <img src="img/bri.png" style="width:100%;" alt="" srcset="">
                </div>
                <div class="col s9">
                    <h6 class="zn-text-black zn-m-0 zn-pb-5">15234124131334
                        <p class="zn-checkbox-tx">
                            <label>
                                <input type="checkbox" class="filled-in" checked="checked" />
                                <span style="padding: 0;"></span>
                            </label>
                        </p>
                    </h6>
                    <h6 class="small zn-text-light zn-m-0">PT. Gema Mandiri Indonesia</h6>
                    <span>
                    </span>
                </div>
            </div>
            <div class="row zn-border-bottom-1 zn-pb-5">
                <div class="col s3">
                    <img src="img/mandiri.png" style="width:100%;" alt="" srcset="">
                </div>
                <div class="col s9">
                    <h6 class="zn-text-black zn-m-0 zn-pb-5">645344123
                        <p class="zn-checkbox-tx">
                            <label>
                                <input type="checkbox" class="filled-in" checked="checked" />
                                <span style="padding: 0;"></span>
                            </label>
                        </p>
                    </h6>
                    <h6 class="small zn-text-light zn-m-0">PT. Gema Mandiri Indonesia</h6>
                    <span>
                    </span>
                </div>
            </div>
        </div>

    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">PILIH BANK ASAL</span>
        <div class="row">
            <div class="input-field col s12">
                <select >
                    <option value="1" selected>BNI</option>
                    <option value="2">BCA</option>
                    <option value="3">MANDIRI</option>
                </select>
            </div>

        </div>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">PEMILIK REKENING BANK ASAL</span>
        <div class="row">
            <div class="col s12">
            <input class="zn-border-bottom-1-light" type="text" placeholder="Nama Pemilik Rekening Bank Asal">
            </div>           
        </div>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">TANGGAL TRANSFER</span>
        <div class="row">
            <div class="col s12">
            <input  type="text" class="datepicker" placeholder="Pilih Tanggal Transfer" class="zn-border-bottom-1-light">
            </div>           
        </div>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label zn-text-light">KETERANGAN</span>
        <div class="row">
            <div class="col s12">
            <input class="zn-border-bottom-1-light" type="text" placeholder="Optional">
            </div>           
        </div>
    </div>

    <div class="zn-con-form">
        <span class="zn-text-11 zn-border-bottom-2 zn-form-label">LAMPIRAN FOTO KTP 
        </span>
        <h6 class="zn-form-label zn-text-light zn-text-medium zn-m-0 zn-p-content">
        <i class="material-icons" style="float: left;">add_a_photo</i>
        <span style="margin-left:10px;margin-top: 8px;display: inline-block;"> Upload</span>
        </h6>
    </div>

    
    <div class="row zn-mlr-10 ">
        <div class="col s12 zn-mt-button zn-mt-15 zn-pb-40">
            <a class="waves-effect waves-light btn zn-button zn-text-medium">KIRIM KONFIRMASI PEMBAYARAN</a>
        </div>
    </div>





    <?php include 'footer.php'; ?>
</body>

</html>