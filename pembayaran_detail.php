<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body class="zn-bg-light">
    <!-- <content> -->
    <div class="navbar-fixed">
    <nav class="nav-extended zn-bg-color-dark z-depth-0">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo zn-text-white" style="left: 50%;">Detail Invoice</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i
                    class="material-icons zn-text-white">arrow_back</i></a>
        </div>
    </nav>
</div>


    <div class="zn-con-form">
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">INVOICE 5555
            <span class="zn-label-active zn-status-btn zn-text-center">validating</span>
        </h6>
        <span class="zn-form-label zn-m-0 zn-pt-5 zn-text-light zn-text-11 ">10 Sep 2019</span>
        <span class="zn-form-label zn-text-light  zn-pb-5 zn-mlr-15 zn-text-11 zn-border-bottom-4" style="display: block;padding: 20px 0px;">METODE PEMBAYARAN</span>
        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">TRANSFER</h6>

        <span class="zn-plr-15 zn-text-11 zn-m-0" style="color:#999999;">DARI:</span>
        <h6 class="zn-plr-15 small zn-m-0">Bank BNI</h6>
        <h6 class="zn-plr-15 small zn-m-0">Oka Oka</h6>
        <span class="zn-plr-15 zn-text-11 zn-m-0 zn-mt-15" style="color:#999999;">KE:</span>
        <h6 class="zn-plr-15 small zn-m-0">Bank BCA 281039180</h6>
        <h6 class="zn-plr-15 small zn-m-0">PT. GEMA Mandiri Indonesia</h6>

        <span class="zn-form-label zn-text-light  zn-pb-5 zn-mlr-15 zn-text-11 zn-border-bottom-4" style="display: block;padding: 20px 0px;">RINCIAN PEMBAYARAN</span>

        <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0">Cicilan Registrasi</h6>
            <div class="row zn-mlr-15">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-light">Cicilan Registrasi 4/5</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">100.000</h6>
                </div>
            </div>

            <div class="row zn-mlr-15 zn-border-top-1">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">100.000</h6>
                </div>
            </div>
            <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Arisan LM</h6>
            <div class="zn-mlr-15 zn-text-11 zn-text-light zn-pb-5">Aug 2019 Kel. 1354 10 gram</div>
            <div class="row zn-mlr-15 zn-pb-5">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
                </div>
            </div>
            <div class="row zn-mlr-15 zn-pb-5">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
                </div>
            </div>
        
            <div class="row zn-mlr-15 zn-border-top-1">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">100.000</h6>
                </div>
            </div>

            <h6 class="zn-form-label zn-color-black zn-text-medium zn-m-0 zn-pb-0">Arisan LM</h6>
            <div class="zn-mlr-15 zn-text-11 zn-text-light zn-pb-5">Aug 2019 Kel. 1354 10 gram</div>
            <div class="row zn-mlr-15 zn-pb-5">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-light zn-m-0">Cicilan Registrasi 4/5</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light zn-m-0">100.000</h6>
                </div>
            </div>
           
          

            <div class="row zn-mlr-15 zn-border-top-1">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Sub Total</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-light">100.000</h6>
                </div>
            </div>

            <div class="row zn-mlr-15 zn-border-top-3">
                <div class="col s6 zn-p-0">
                    <h6 class="small zn-text-right zn-text-black zn-text-medium">Total</h6>
                </div>
                <div class="col s1 zn-p-0">
                    <h6 class="small zn-text-right zn-text-black zn-text-medium">Rp.</h6>
                </div>
                <div class="col s5 zn-p-0">
                    <h6 class="small zn-text-right zn-text-black zn-text-medium">100.000</h6>
                </div>
            </div>
    </div>





    <?php include 'footer.php'; ?>
</body>

</html>